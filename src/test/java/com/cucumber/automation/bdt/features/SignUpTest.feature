Feature: As an user, I should be able to sign up to express

  @Successful
  Scenario: User sign up
    Given I am on the home page
    When  I click on sign in button
    Then  I click on sign up button
    And   I enter email
    And   I enter first name as "Donald"
    And   I enter last name as "Trump"
    And   I enter password as "Dtrump101"
    And   I select "Australia" as country
