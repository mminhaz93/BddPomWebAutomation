Feature: As an user, I should be able to look up my previous order

  @Successful @negative-testing
  Scenario: User looks up invalid order
    Given I am on the home page
    When  I click on sign in button
    Then  I click on find an order
    And   I enter order number as "orderNum123"
    And   I enter last name in order page as "Trump"
    When  I click find order button
    Then  I should see error message "No results found for that combination. Please try again."

  @Successful
  Scenario: User is able see existing account prompt
    Given I am on the home page
    When  I click on sign in button
    Then  I click on find an order
    And   I should see "Have an Express account?" prompt
    And   I should see "Use your Express account to view your order history and account information." validation message

  @Successful
  Scenario: User is able to navigate back to sign in page
    Given I am on the home page
    When  I click on sign in button
    Then  I click on find an order
    When  I click sign in button from order page
    Then  I should be in sign in page
    And   I should see email field
    And   I should see password field
    And   I should see forgot password link