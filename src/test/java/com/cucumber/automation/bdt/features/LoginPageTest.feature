Feature: As an user, I should be able view sign in page and its contents

  @Successful
  Scenario: User finds welcome back message
    Given I am on the home page
    When  I click on sign in button
    Then  I should see "Welcome back" title
    And   I should see "Sign in to your account to see your Rewards & checkout faster." message

  @Successful
  Scenario: User navigates to Sign In / Sign Up page
    Given I am on the home page
    When  I click on sign in button
    Then  I should be in sign in page
    And   I should see email field
    And   I should see password field
    And   I should see forgot password link