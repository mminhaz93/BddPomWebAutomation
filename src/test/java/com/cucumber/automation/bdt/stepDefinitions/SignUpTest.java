package com.cucumber.automation.bdt.stepDefinitions;

import java.util.UUID;
import com.cucumber.automation.web.pages.signUpPage;
import cucumber.api.java.en.Then;

public class SignUpTest extends signUpPage{

    String emailPrefix = UUID.randomUUID().toString();
    String email = "dtrump1"+emailPrefix+"@gmail.com";

    @Then("^I click on sign up button$")
    public void i_click_on_sign_up_button(){
        clickSignUpButton();
    }

    @Then("^I enter email$")
    public void i_enter_email(){
        enterEmail(email);
    }

    @Then("^I enter first name as \"([^\"]*)\"$")
    public void i_enter_first_name_as(String firstName){
        enterFirstName(firstName);
    }

    @Then("^I enter last name as \"([^\"]*)\"$")
    public void i_enter_last_name_as(String lastName){
        enterLastName(lastName);
    }

    @Then("^I enter password as \"([^\"]*)\"$")
    public void i_enter_password_as(String password){
        enterPassword(password);
    }

    @Then("^I select \"([^\"]*)\" as country$")
    public void i_select_as_country(String country){
        selectCountry(country);
    }

}

