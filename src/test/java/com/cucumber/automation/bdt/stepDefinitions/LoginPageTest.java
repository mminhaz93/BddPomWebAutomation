package com.cucumber.automation.bdt.stepDefinitions;

import com.cucumber.automation.utils.TestUtil;
import com.cucumber.automation.web.pages.homePage;
import com.cucumber.automation.web.pages.loginPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPageTest extends loginPage {

    homePage page = new homePage();
    TestUtil util = new TestUtil();

    @Given("^I am on the home page$")
    public void i_am_on_the_home_page(){
        page.isUserOnHomePage();
    }

    @When("^I click on sign in button$")
    public void i_click_on_sign_in_button(){
        clickSignInButton();
    }

    @Then("^I should be in sign in page$")
    public void i_should_be_in_sign_in_page(){
        isInSignInPage();
    }

    @Then("^I should see \"([^\"]*)\" title$")
    public void i_should_see_title(String welcomeTitle) {
        WebElement welcomeTitleElement = driver.findElement(By.className("siyo8"));
        util.isTextDisplayed(welcomeTitleElement, welcomeTitle);
    }

    @Then("^I should see \"([^\"]*)\" message$")
    public void i_should_see_message(String welcomeMessage){
        WebElement welcomeMsgElement = driver.findElement(By.className("EtX3L"));
        util.isTextDisplayed(welcomeMsgElement, welcomeMessage);
    }

    @Then("^I should see email field$")
    public void i_should_see_email_field(){
        emailFieldDisplayed();
    }

    @Then("^I should see password field$")
    public void i_should_see_password_field(){
        passwordFieldDisplayed();
    }

    @Then("^I should see forgot password link$")
    public void i_should_see_forgot_password_link(){
        forgotLinkDisplayed();
    }

    // LoginTest.feature step
    @When("^I enter email as \"([^\"]*)\"$")
    public void i_enter_email(String email){
        enterLoginEmail(email);
    }

}
