package com.cucumber.automation.bdt.stepDefinitions;

import com.cucumber.automation.utils.TestUtil;
import com.cucumber.automation.web.pages.orderPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FindOrderTest extends orderPage {

    TestUtil util = new TestUtil();

    @When("^I click on find an order$")
    public void i_click_on_find_an_order(){
        clickFindFindOrder();
    }
 
    @Then("^I enter order number as \"([^\"]*)\"$")
    public void i_enter_order_number_as(String orderNumber){
        enterOrderNumber(orderNumber);
    }
    
    @Then("^I enter last name in order page as \"([^\"]*)\"$")
    public void i_enter_last_name_in_order_page_as(String lastName){
        enterLastName(lastName);
    }
    
    @Then("^I click find order button$")
    public void i_click_find_order_button(){
        clickFindOrderButton();
    }
    
    @Then("^I should see error message \"([^\"]*)\"$")
    public void i_should_see_error_message(String erroMessage){
        orderErrorMessage(erroMessage);
    }

    @Then("^I should see \"([^\"]*)\" prompt$")
    public void i_should_see_prompt(String accountPrompt){
        existingAccount(accountPrompt);
    }

    @Then("^I should see \"([^\"]*)\" validation message$")
    public void i_should_see_validation_message(String orderHistoryMessage){
        exisitingAccountMessage(orderHistoryMessage);
    }

    @When("^I click sign in button from order page$")
    public void i_click_sign_in_button_from_order_page(){
        clickSignIn();
    }
}
