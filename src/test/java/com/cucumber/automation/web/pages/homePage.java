package com.cucumber.automation.web.pages;

import com.cucumber.automation.utils.TestBase;

import static org.junit.Assert.assertEquals;

public class homePage extends TestBase {

    public void isUserOnHomePage(){
        String actualTitle = driver.getTitle();
        String expectedTitle = "Men's and Women's Clothing - Shop jeans, dresses, and suits";
        assertEquals(expectedTitle,actualTitle);
    }
}
