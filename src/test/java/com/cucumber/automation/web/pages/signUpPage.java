package com.cucumber.automation.web.pages;

import com.cucumber.automation.utils.TestBase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class signUpPage extends TestBase{
    public void clickSignUpButton() {
        driver.findElement(By.cssSelector("[data-selected='false']")).click();
    }

    public void enterEmail(String email) {
        WebElement emailElm = driver.findElement(By.cssSelector("input[name='loginName']"));
        emailElm.click();
        emailElm.sendKeys(email);
    }

    public void enterFirstName(String firstName) {
        WebElement firstNameElm = driver.findElement(By.cssSelector("input[name='firstname']"));
        firstNameElm.click();
        firstNameElm.sendKeys(firstName);
    }

    public void enterLastName(String lastName) {
        WebElement lastNameElm = driver.findElement(By.cssSelector("input[name='lastname']"));
        lastNameElm.click();
        lastNameElm.sendKeys(lastName);
    }

    public void enterPassword(String password) {
        WebElement passwordNameElm = driver.findElement(By.cssSelector("input[name='password']"));
        passwordNameElm.click();
        passwordNameElm.sendKeys(password);
    }

    public void selectCountry(String country) {
        new Select(driver.findElement(By.cssSelector("[name='country']"))).selectByVisibleText(country);
    }
}
