package com.cucumber.automation.web.pages;

import com.cucumber.automation.utils.TestBase;
import com.cucumber.automation.utils.TestUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class orderPage extends TestBase{
    TestUtil util = new TestUtil();

    public void clickFindFindOrder() {
        driver.findElement(By.cssSelector(".grid a[href='/status/order-status.jsp']")).click();
    }

    public void enterOrderNumber(String orderNumber) {
        WebElement orderNumberElm = driver.findElement(By.cssSelector("input[name='order-number']"));
        orderNumberElm.click();
        orderNumberElm.sendKeys(orderNumber);
    }

    public void enterLastName(String lastName) {
        WebElement lastNameElm = driver.findElement(By.cssSelector("input[name='last-name']"));
        lastNameElm.click();
        lastNameElm.sendKeys(lastName);
    }

    public void clickFindOrderButton() {
        driver.findElement(By.cssSelector("input[value='Find Order']")).click();
    }

    public void orderErrorMessage(String erroMessage) {
        String actualErroMsg = driver.findElement(By.className("online-orders-error-msg")).getText();
        Assert.assertEquals(actualErroMsg.toLowerCase(), erroMessage.toLowerCase());
    }

    public void clickSignIn() {
        driver.findElement(By.cssSelector(".cta.sign-in")).click();
    }

    public void existingAccount(String accountPrompt) {
        WebElement expressAccountElement = driver.findElement(By.cssSelector(".signin-container .find-your-order"));
        util.isTextDisplayed(expressAccountElement, accountPrompt);
    }

    public void exisitingAccountMessage(String orderHistoryMessage) {
        WebElement expressAccountMessageElement = driver.findElement(By.cssSelector(".signin-container .acc-info"));
        util.isTextDisplayed(expressAccountMessageElement, orderHistoryMessage);
    }
}
