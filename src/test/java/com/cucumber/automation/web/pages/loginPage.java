package com.cucumber.automation.web.pages;

import com.cucumber.automation.utils.TestBase;
import com.cucumber.automation.utils.TestUtil;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public class loginPage extends TestBase{
    TestUtil util = new TestUtil();

    WebDriverWait wait = new WebDriverWait(driver, 10);

    public void clickSignInButton() {
        driver.findElement(By.cssSelector(".login-item.signed-out")).click();
    }

    public void isInSignInPage() {
        String loginTitle = driver.getCurrentUrl();
        Assert.assertTrue(loginTitle.contains("login"));
    }

    public void emailFieldDisplayed() {
        Assert.assertTrue("Email field is not found", driver.findElement(By.id("login-form-email-addr")).isDisplayed());
    }

    public void passwordFieldDisplayed() {
        Assert.assertTrue("Password field is not found", driver.findElement(By.name("password")).isDisplayed());
    }

    public void forgotLinkDisplayed() {
        Assert.assertTrue("Forgot password link is not found", driver.findElement(By.className("linkDark")).isDisplayed());
    }

    public void isInputVisible(String cssSelector){
        WebElement inputSelector = driver.findElement(By.cssSelector(cssSelector));
        Boolean inputDisplayed = inputSelector.isDisplayed();
        String inputText = inputSelector.getText();
        Assert.assertTrue("Input " + inputText + " wasn't displayed", inputDisplayed);
    }

    public void enterLoginEmail(String email) {
        WebElement emailElm = driver.findElement(By.cssSelector("input[name='email']"));
        emailElm.click();
        emailElm.sendKeys(email);
    }


}
