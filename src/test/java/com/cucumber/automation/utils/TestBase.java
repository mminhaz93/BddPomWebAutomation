package com.cucumber.automation.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {

	public static WebDriver driver = null;
	public static WebDriverWait waitVar = null;
    public static Properties prop;

    //Browserstack config
    public static final String USERNAME = "mohammedminhaz2";
    public static final String AUTOMATE_KEY = "9icfYVQksbcEGzcw8QpG";
    public static final String btURL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

    public TestBase(){
        try {
            prop = new Properties();
            //Reading from property file
            FileInputStream ip = new FileInputStream(System.getProperty("user.dir") + "/src/test/java/com/cucumber/automation/config/config.properties");
            prop.load(ip);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	/**
	 *  This function is to invoke Selenium Webdriver
	 * 
	 * @throws MalformedURLException
	 * @throws InterruptedException
	 */
	public void createDriver() throws MalformedURLException, InterruptedException {
        System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/src/test/java/com/cucumber/automation/config/chromedriver");
        System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir") +"/src/test/java/com/cucumber/automation/config/geckodriver");

        String browserName = prop.getProperty("browser");

        if(browserName.equals("chrome")){
            driver = new ChromeDriver();
        }
        else if(browserName.equals("firefox")){
            driver = new FirefoxDriver();
        }
        else if(browserName.equals("safari")){
            driver = new SafariDriver();
        }
        else if(browserName.equals("all")){
            driver = new ChromeDriver();
            driver = new FirefoxDriver();
            driver = new SafariDriver();
        }
        else if(browserName.equals("browserstack")){
            // IE 11 window 8.1
            DesiredCapabilities caps = new DesiredCapabilities();
            caps.setCapability("browser", "IE");
            caps.setCapability("browser_version", "11.0");
            caps.setCapability("os", "Windows");
            caps.setCapability("os_version", "8.1");
            caps.setCapability("resolution", "1024x768");
            // Edge window 10
            DesiredCapabilities caps2 = new DesiredCapabilities();
            caps2.setCapability("os", "Windows");
            caps2.setCapability("os_version", "10");
            caps2.setCapability("browser", "Edge");
            caps2.setCapability("browser_version", "insider preview");
            caps2.setCapability("browserstack.local", "false");
            caps2.setCapability("browserstack.selenium_version", "3.5.2");
            //Chrome windows 7
            DesiredCapabilities caps3 = new DesiredCapabilities();
            caps3.setCapability("os", "Windows");
            caps3.setCapability("os_version", "7");
            caps3.setCapability("browser", "Chrome");
            caps3.setCapability("browser_version", "67.0");
            caps3.setCapability("browserstack.local", "false");
            caps3.setCapability("browserstack.selenium_version", "3.5.2");

            driver = new RemoteWebDriver(new URL(btURL), caps);
            driver = new RemoteWebDriver(new URL(btURL), caps2);
            driver = new RemoteWebDriver(new URL(btURL), caps2);
        }

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        //Passing url from property file
        driver.get(prop.getProperty("url"));
		waitVar = new WebDriverWait(driver, 15);
	}

	/**
	 * This function is to close driver instance
	 */
	public void teardown() {
			driver.quit();
	}
}
