package com.cucumber.automation.utils;

import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class TestUtil extends TestBase{

    public void isTextDisplayed(WebElement element, String expectedText){
        if (element.isDisplayed()){
            String textDisplayed = element.getText();
            Assert.assertEquals(textDisplayed, expectedText);
        }else {
            Assert.assertTrue(element.getText() + " is not displayed", false);
        }

    }
}
