$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("FindOrderTest.feature");
formatter.feature({
  "line": 1,
  "name": "As an user, I should be able to look up my previous order",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-look-up-my-previous-order",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4870089369,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "User looks up invalid order",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-look-up-my-previous-order;user-looks-up-invalid-order",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Successful"
    },
    {
      "line": 3,
      "name": "@negative-testing"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I click on find an order",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I enter order number as \"orderNum123\"",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I enter last name in order page as \"Trump\"",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I click find order button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should see error message \"No results found for that combination. Please try again.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 183899125,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2611528042,
  "status": "passed"
});
formatter.match({
  "location": "FindOrderTest.i_click_on_find_an_order()"
});
formatter.result({
  "duration": 1316855109,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "orderNum123",
      "offset": 25
    }
  ],
  "location": "FindOrderTest.i_enter_order_number_as(String)"
});
formatter.result({
  "duration": 285197171,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Trump",
      "offset": 36
    }
  ],
  "location": "FindOrderTest.i_enter_last_name_in_order_page_as(String)"
});
formatter.result({
  "duration": 188494606,
  "status": "passed"
});
formatter.match({
  "location": "FindOrderTest.i_click_find_order_button()"
});
formatter.result({
  "duration": 1585924677,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "No results found for that combination. Please try again.",
      "offset": 28
    }
  ],
  "location": "FindOrderTest.i_should_see_error_message(String)"
});
formatter.result({
  "duration": 35299888,
  "status": "passed"
});
formatter.after({
  "duration": 105077617,
  "status": "passed"
});
formatter.before({
  "duration": 3280465981,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "User is able see existing account prompt",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-look-up-my-previous-order;user-is-able-see-existing-account-prompt",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 13,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 15,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "I click on find an order",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I should see \"Have an Express account?\" prompt",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "I should see \"Use your Express account to view your order history and account information.\" validation message",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 101515443,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2292718444,
  "status": "passed"
});
formatter.match({
  "location": "FindOrderTest.i_click_on_find_an_order()"
});
formatter.result({
  "duration": 1997528400,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Have an Express account?",
      "offset": 14
    }
  ],
  "location": "FindOrderTest.i_should_see_prompt(String)"
});
formatter.result({
  "duration": 60441846,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Use your Express account to view your order history and account information.",
      "offset": 14
    }
  ],
  "location": "FindOrderTest.i_should_see_validation_message(String)"
});
formatter.result({
  "duration": 84294356,
  "status": "passed"
});
formatter.after({
  "duration": 130792585,
  "status": "passed"
});
formatter.before({
  "duration": 4985999085,
  "status": "passed"
});
formatter.scenario({
  "line": 22,
  "name": "User is able to navigate back to sign in page",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-look-up-my-previous-order;user-is-able-to-navigate-back-to-sign-in-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 21,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 23,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 25,
  "name": "I click on find an order",
  "keyword": "Then "
});
formatter.step({
  "line": 26,
  "name": "I click sign in button from order page",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "I should be in sign in page",
  "keyword": "Then "
});
formatter.step({
  "line": 28,
  "name": "I should see email field",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I should see password field",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I should see forgot password link",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 96296215,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2739301072,
  "status": "passed"
});
formatter.match({
  "location": "FindOrderTest.i_click_on_find_an_order()"
});
formatter.result({
  "duration": 1509131700,
  "status": "passed"
});
formatter.match({
  "location": "FindOrderTest.i_click_sign_in_button_from_order_page()"
});
formatter.result({
  "duration": 1886648995,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_be_in_sign_in_page()"
});
formatter.result({
  "duration": 7210231,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_email_field()"
});
formatter.result({
  "duration": 39854067,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_password_field()"
});
formatter.result({
  "duration": 48416003,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_forgot_password_link()"
});
formatter.result({
  "duration": 46972483,
  "status": "passed"
});
formatter.after({
  "duration": 114663392,
  "status": "passed"
});
formatter.uri("LoginPageTest.feature");
formatter.feature({
  "line": 1,
  "name": "As an user, I should be able view sign in page and its contents",
  "description": "",
  "id": "as-an-user,-i-should-be-able-view-sign-in-page-and-its-contents",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4160342787,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "User finds welcome back message",
  "description": "",
  "id": "as-an-user,-i-should-be-able-view-sign-in-page-and-its-contents;user-finds-welcome-back-message",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see \"Welcome back\" title",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I should see \"Sign in to your account to see your Rewards \u0026 checkout faster.\" message",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 6917828,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 3554602475,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Welcome back",
      "offset": 14
    }
  ],
  "location": "LoginPageTest.i_should_see_title(String)"
});
formatter.result({
  "duration": 59267453,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sign in to your account to see your Rewards \u0026 checkout faster.",
      "offset": 14
    }
  ],
  "location": "LoginPageTest.i_should_see_message(String)"
});
formatter.result({
  "duration": 59989498,
  "status": "passed"
});
formatter.after({
  "duration": 99900769,
  "status": "passed"
});
formatter.before({
  "duration": 3240231966,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "User navigates to Sign In / Sign Up page",
  "description": "",
  "id": "as-an-user,-i-should-be-able-view-sign-in-page-and-its-contents;user-navigates-to-sign-in-/-sign-up-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "I should be in sign in page",
  "keyword": "Then "
});
formatter.step({
  "line": 15,
  "name": "I should see email field",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I should see password field",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I should see forgot password link",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 27067710,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2921570033,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_be_in_sign_in_page()"
});
formatter.result({
  "duration": 10889291,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_email_field()"
});
formatter.result({
  "duration": 44573657,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_password_field()"
});
formatter.result({
  "duration": 39382448,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_should_see_forgot_password_link()"
});
formatter.result({
  "duration": 45498905,
  "status": "passed"
});
formatter.after({
  "duration": 104498155,
  "status": "passed"
});
formatter.uri("LoginTest.feature");
formatter.feature({
  "line": 1,
  "name": "As an user, I should be able to log in my express account",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-log-in-my-express-account",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4741820793,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "User sign in",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-log-in-my-express-account;user-sign-in",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I enter email as \"dtrump101@gmail.com\"",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "I enter password as \"Dtrump101\"",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 78842324,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2152592527,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "dtrump101@gmail.com",
      "offset": 18
    }
  ],
  "location": "LoginPageTest.i_enter_email(String)"
});
formatter.result({
  "duration": 285489020,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dtrump101",
      "offset": 21
    }
  ],
  "location": "SignUpTest.i_enter_password_as(String)"
});
formatter.result({
  "duration": 174754013,
  "status": "passed"
});
formatter.after({
  "duration": 107634791,
  "status": "passed"
});
formatter.uri("SignUpTest.feature");
formatter.feature({
  "line": 1,
  "name": "As an user, I should be able to sign up to express",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-sign-up-to-express",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3259588378,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "User sign up",
  "description": "",
  "id": "as-an-user,-i-should-be-able-to-sign-up-to-express;user-sign-up",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Successful"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am on the home page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I click on sign in button",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I click on sign up button",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I enter email",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I enter first name as \"Donald\"",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter last name as \"Trump\"",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I enter password as \"Dtrump101\"",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I select \"Australia\" as country",
  "keyword": "And "
});
formatter.match({
  "location": "LoginPageTest.i_am_on_the_home_page()"
});
formatter.result({
  "duration": 17435487,
  "status": "passed"
});
formatter.match({
  "location": "LoginPageTest.i_click_on_sign_in_button()"
});
formatter.result({
  "duration": 2180420247,
  "status": "passed"
});
formatter.match({
  "location": "SignUpTest.i_click_on_sign_up_button()"
});
formatter.result({
  "duration": 461213440,
  "status": "passed"
});
formatter.match({
  "location": "SignUpTest.i_enter_email()"
});
formatter.result({
  "duration": 655137093,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Donald",
      "offset": 23
    }
  ],
  "location": "SignUpTest.i_enter_first_name_as(String)"
});
formatter.result({
  "duration": 164705323,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Trump",
      "offset": 22
    }
  ],
  "location": "SignUpTest.i_enter_last_name_as(String)"
});
formatter.result({
  "duration": 148860112,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Dtrump101",
      "offset": 21
    }
  ],
  "location": "SignUpTest.i_enter_password_as(String)"
});
formatter.result({
  "duration": 178178352,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Australia",
      "offset": 10
    }
  ],
  "location": "SignUpTest.i_select_as_country(String)"
});
formatter.result({
  "duration": 126268941,
  "status": "passed"
});
formatter.after({
  "duration": 89017816,
  "status": "passed"
});
});